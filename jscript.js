// Завдання №1:

// спосіб №1
let str1 = "my name Sasha"
let newStr1 = str1[0].toUpperCase() + str1.slice(1);
console.log(newStr1)

// спосіб №2
let str2 = `my name sasha`
function newStr2(str2) {
    if (!str2) return str2;
    return str2[0].toUpperCase() + str2.slice(1);
}
console.log(newStr2(str2))

//--------------------------------------------------------------------------------

// Завдання №2

// спосіб №1
let str3 = "123456"
function reverseString1(str3) {
    return str3.split("").reverse().join("");
}
console.log(reverseString1(str3))

// спосіб №2
function reverseString2(str3) {
    let newString = "";
    for (let i = str3.length - 1; i >= 0; i--) {
        newString += str3[i];
    }
    return newString;
}
console.log(reverseString2(str3))

//-------------------------------------------------------------------------------

// Завдання №3
let find1 = "http://something.com";
function myFunction1(find1) {
    let result
    result = find1.startsWith("http://");
    return result
}
console.log(myFunction1(find1))

//-------------------------------------------------------------------------------

// Завдання №4
let find2 = "some file with doc.html";
function myFunction2(find2) {
    let result
    result = find2.endsWith(".html");
    return result
}
console.log(myFunction2(find2))

